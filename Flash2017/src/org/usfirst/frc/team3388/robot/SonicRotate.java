package org.usfirst.frc.team3388.robot;

import edu.flash3388.flashlib.math.Mathf;
import edu.flash3388.flashlib.robot.Action;
import edu.flash3388.flashlib.robot.System;
import edu.flash3388.flashlib.robot.devices.RangeFinder;
import edu.flash3388.flashlib.robot.systems.ModableMotor;
import edu.flash3388.flashlib.robot.systems.Rotatable;
import edu.flash3388.flashlib.util.FlashUtil;

public class SonicRotate extends Action{
	
	public static final double ACCURACY_MARGIN = 5.0;
	public static final long ACTION_VALIDATION_TIMEOUT = 50;
	public static final double DEFAULT_MIN_SPEED = 0.15;
	public static final double DEFAULT_MAX_SPEED = 0.8;
	
	private Rotatable driveTrain;
	private RangeFinder finderR, finderL;
	private ModableMotor modable;
	private boolean centered;
	private double speed, minSpeed, maxSpeed, baseSpeed,
				   margin, lastPixels, pixelDifference, differences;
	private int lastDir;
	private long centeredTimeout, timeCentered;
	
	public SonicRotate(Rotatable driveTrain, RangeFinder finderR, RangeFinder finderL, double speed, double margin, long centeredTime){
		this.driveTrain = driveTrain;
		this.baseSpeed = speed;
		this.margin = margin;
		this.centeredTimeout = centeredTime;
		this.finderL = finderL;
		this.finderR = finderR;
		
		System s = null;
		if((s = driveTrain.getSystem()) != null)
			requires(s);
		if(driveTrain instanceof ModableMotor)
			modable = (ModableMotor)driveTrain;
	}
	public SonicRotate(Rotatable driveTrain, RangeFinder finderR, RangeFinder finderL, double speed){
		this(driveTrain, finderR, finderL, speed, ACCURACY_MARGIN, ACTION_VALIDATION_TIMEOUT);
	}
	
	@Override
	protected void initialize() {
		centered = false;
		timeCentered = -1;
		speed = baseSpeed;
		lastPixels = -1;
		pixelDifference = 0;
		differences = 0;
		
		if(minSpeed <= 0)
			minSpeed = DEFAULT_MIN_SPEED;
		if(maxSpeed > 1 || maxSpeed <= 0)
			maxSpeed = DEFAULT_MAX_SPEED;
		
		if(modable != null && modable.inBrakeMode())
			modable.enableBrakeMode(false);
	}
	@Override
	protected void execute() {
		int dir = 1; 
		double rotateSpeed = 0;
		double offset = finderR.getRangeCM() - finderL.getRangeCM();
		
		dir = offset > 0 ? 1 : -1;
		offset = Math.abs(offset);
		
		if(offset > -margin && offset < margin){//centered on target
			rotateSpeed = 0;
			if(modable != null && !modable.inBrakeMode())
				modable.enableBrakeMode(true);
			if(!centered){
				centered = true;
				timeCentered = FlashUtil.millis();
			}
		}else{//not centered on target
			centered = false;
			timeCentered = -1;
			rotateSpeed = speed * (offset / 100.0);
			rotateSpeed = Mathf.limit(rotateSpeed, minSpeed, maxSpeed);
		}
		
		if(lastPixels >= 0){
			pixelDifference = Math.abs(offset - lastPixels);
			differences++;
		}
		lastPixels = offset;
		if(lastDir != dir){
			speed /= 1.2;
			rotateSpeed /= 1.2;
			FlashUtil.getLog().log("Lowering Speed");
		}
		if(differences > 0 && Math.abs(pixelDifference - lastPixels) <= margin){
			rotateSpeed = 0;
			FlashUtil.getLog().log("Predicted Stop");
		}
		FlashUtil.getLog().log("Speed: "+rotateSpeed+" Dir: "+dir+" Differences: "+differences+" PixelsD: "+pixelDifference + " L: "+lastPixels);
		driveTrain.rotate(rotateSpeed, dir);
		lastDir = dir;
	}
	@Override
	protected boolean isFinished() {
		long millis = FlashUtil.millis();
		return (finiteCenteredTimeout() && isCentered() && millis - timeCentered >= centeredTimeout);
	}
	@Override
	protected void end() {
		driveTrain.stop();
		if (modable != null && modable.inBrakeMode())
			modable.enableBrakeMode(false);
	}

	public void setMotorModeSource(ModableMotor modable){
		this.modable = modable;
	}
	public double getSpeed(){
		return baseSpeed;
	}
	public void setSpeed(double sp){
		baseSpeed = sp;
	}
	public double getMinSpeed(){
		return minSpeed;
	}
	public void setMinSpeed(double min){
		minSpeed = min;
	}
	public double getMaxSpeed(){
		return maxSpeed;
	}
	public void setMaxSpeed(double max){
		minSpeed = max;
	}
	
	public boolean finiteCenteredTimeout(){
		return centeredTimeout > 0;
	}
	public boolean isCentered(){
		return centered;
	}
	public double getPixelMargin(){
		return margin;
	}
	public void setPixelMargin(double pixels){
		margin = pixels;
	}
	public long getCenterTimeout(){
		return centeredTimeout;
	}
	public void setCenterTimeout(long millis){
		centeredTimeout = millis;
	}
}
